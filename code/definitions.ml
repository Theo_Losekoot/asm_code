open Generated

(* the identity monad, stolen from https://gitlab.inria.fr/skeletons/necro-ml/ *)
module ID =
struct
	exception Branch_fail of string
	type 'a t = 'a
	let ret x = x
	let rec branch l =
		begin match l with
		| [] -> raise (Branch_fail "No branch matches")
		| b1 :: bq ->
				try b1 () with Branch_fail _ -> branch bq
		end
	let fail s = raise (Branch_fail s)
	let bind x f = f x
	let apply f x = f x
end

(* implementaiton of missing types *)
module rec Types :
sig
  type ident = string
  (* an environment is simply a list of (key, value) *)
  type env = (ident * Unspec(ID)(Types).semiValue) list
  type location = int
  (* a memory is simply a list of (location, value) *)
  type memory = (location * Unspec(ID)(Types).state) list
end = Types

(* This module defines all missing functions. It is not very generic 
	monad-wise, but works for the identity monad. *)
module Spec =
struct
  include Unspec(ID)(Types)
  open Types

  (* To write a value in memry, just append it in front of the rest. It 
	shadows all other possible definitions of the same variable.
	This is not efficient memory-wise, but that wasn't the goal here. *)
  let addEnv (env : env) (id : ident) (sv : semiValue) : env = (id, sv) :: env
  let rec getEnv (env : env) (id : ident) =
    match env with
    | [] -> M.fail (Printf.sprintf "Tried to read uninitialized variable %s in environment" id)
    | (idh, svh) :: t ->
      if idh = id
      then svh
      else getEnv t id

  (* the memory is handled in the same way *)
  let writeMem (mem : memory) (loc : location) (st : state) =
    (loc, st) :: mem
  let rec readMem (mem : memory) (loc : location) =
		match mem with
    | [] -> M.fail (Printf.sprintf "Tried to read uninitialized location %i in memory" loc)
    | (loch, sth) :: t ->
      if loch = loc
      then M.ret sth
      else readMem t loc

  (* fresh rever returns two times the same value (except if you exceed max_int) *)
  let index = ref 0
  let fresh () : location M.t =
    index := !index + 1 ;
    M.ret !index
end

(* Our beautiful interpreter *)
module Interpreter = MakeInterpreter (Spec)

open Types
open Spec
open Interpreter

(* steps until blocking configuration is reached *)
let rec run (conf : configuration) =
  try
    run (step conf)
  with
  | ID.Branch_fail _ -> conf

(* evaluate a single term in an empty environment, in an empty memory *)
let eval (t : lterm) =
  run (Conf ([], Context ([], t)))


(* all of the following functions are for pretty-printing. You can stop reading this file here. *)
let print_list elmt_printer l chan =
  match l with
  | [] -> Format.fprintf chan "[]"
  | x :: xq ->
    Format.fprintf chan "[%t %t]"
      (elmt_printer x)
      (fun chan ->
        (List.iter
         (fun x -> Format.fprintf chan ";@. %t" (elmt_printer x))
          xq
        )
      )

let rec print_memory mem chan =
  print_list print_mem_elmt mem chan

and print_env env chan =
  print_list print_env_elmt env chan

and print_env_elmt env_elmt chan =
  let (id, sv) = env_elmt in
  Format.fprintf chan "%s = %t" id (print_semi_value sv)

and print_mem_elmt mem_elmt chan =
  let (i, st) = mem_elmt in
  Format.fprintf chan "%i <- %t" i (print_state st)

and print_value v chan =
  match v with
  | TailV -> Format.fprintf chan "tail"
  | HeadV -> Format.fprintf chan "head"
  | ListV lv -> print_listv lv chan
  | Clos (sigma, var, body) -> Format.fprintf chan "(%t, %s, %t)" (print_env sigma) var (print_term body)

and print_semi_value sv chan =
  match sv with
  | Loc i -> Format.fprintf chan "%i" i
  | SemiCons(svh, svt) -> Format.fprintf chan "Cons(%t, %t)" (print_semi_value svh) (print_semi_value svt)
  | Value v -> print_value v chan

and print_listv l chan =
  match l with
  | NilV -> Format.fprintf chan "Nil"
  | ConsV (hd, tl) -> Format.fprintf chan "Cons(%t, %t)" (print_value hd) (print_listv tl)

and print_listt l chan =
  match l with
  | Nil -> Format.fprintf chan "Nil"
  | Cons (hd, tl) -> Format.fprintf chan "Cons(%t, %t)" (print_term hd) (print_listt tl)

and print_term t chan =
  match t with
  | Var id -> Format.fprintf chan "%s" id
  | Tail -> Format.fprintf chan "tail"
  | Head -> Format.fprintf chan "head"
  | List l -> print_listt l chan
  | App (f, arg) -> Format.fprintf chan "%t %t" (print_term f) (print_term arg)
  | Abs (var, body) -> Format.fprintf chan "Lam %s. %t" var (print_term body)

and print_state st chan =
  match st with
  | Context (sigma, t) ->
    Format.fprintf chan "(%t, %t)" (print_env sigma) (print_term t)
  | AppSt (st', sv) ->
    Format.fprintf chan "%t %t" (print_state st') (print_semi_value sv)
  | SemiValue sv -> print_semi_value sv chan

and print_conf conf chan =
  match conf with
  | Conf (mem, st) ->
    Format.fprintf chan "%t,@.%t\n" (print_memory mem) (print_state st)
