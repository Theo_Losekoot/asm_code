open Definitions

open Types
open Spec
open Interpreter

(* Test1, same as the second example of our report. *)
let id = Abs ("x", Var "x")
let self = Abs ("x", App (Var "x", Var "x"))
let omega = App (self, self)

let list1 = List (Cons (id, Cons (omega, Nil)))
let list2 = List (Cons (omega, Cons (self, Nil)))
let list_arg = List (Cons(list1, Cons (list2, Nil)))
let list_fun = List (Cons(Head, Cons (Tail, Nil)))
let test = App(list_fun, list_arg)



(* Church number n with customizable function and initiator symbol. (used to define the second example) *)
let church fun_symbol arg_symbol n =
  let rec church_aux fun_symbol arg_symbol n =
    if n <= 0
    then Var arg_symbol
    else App (Var fun_symbol, (church_aux fun_symbol arg_symbol (n-1)))
  in
  Abs (fun_symbol, Abs(arg_symbol, (church_aux fun_symbol arg_symbol n)))

let apply_f_to_x_10_times = church "f" "x" 10

(* auxiliary functions for second example *)
let range n = List.init n (fun x -> x)
let conshead lt = Cons (Head, lt)
let list11 =
  List (
  List.fold_left
    (fun lt i -> conshead lt)
    Nil
    (range 11)
  )
(* applies ten times the tail function on an eleven-sized list of heads *)
let test2 = App (App (apply_f_to_x_10_times, Tail), list11)

(* automatically prints a nice formatting for each example *)
let count = ref 1
let tester term =
  Format.printf "\n-------------\n--- TEST%i ---\n-------------\n" !count;
  Format.printf "Term: %t@.is evaluated as:@." (print_term term);
  begin try
    print_conf (eval term) Format.std_formatter
  with
  | Stack_overflow -> Format.printf "We ran into an overflow!!@."
  end;
  count := !count + 1


(* Test suite *)
let _ = Format.printf "Test suite:\n"
let _ = tester test
let _ = tester test2
let _ = tester omega
let _ = Format.printf "\nDONE.\n"
